package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

}
