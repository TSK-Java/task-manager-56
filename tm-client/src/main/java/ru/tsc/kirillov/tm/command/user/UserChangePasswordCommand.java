package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.UserChangePasswordRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "change-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Изменение пароля пользователя]");
        System.out.println("Введите новый пароль");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), newPassword);
        getUserEndpoint().changePassword(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
