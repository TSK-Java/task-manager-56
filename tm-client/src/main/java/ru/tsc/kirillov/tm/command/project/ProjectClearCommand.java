package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectClearRequest;

@Component
public final class ProjectClearCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить все проекты.";
    }

    @Override
    public void execute() {
        System.out.println("[Очистка списка проектов]");
        getProjectTaskEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

}
