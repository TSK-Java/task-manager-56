package ru.tsc.kirillov.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.service.ICommandService;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.FileEventKind;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileCommand {

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @Nullable
    private FileWatcher watcher;

    @NotNull
    final File folder = new File("./Command");

    private void init() {
        @NotNull Iterable<AbstractCommand> commands = commandService.getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        prepareFolder(folder);
        watcher = new FileWatcher(folder);
        watcher.addListener(event -> {
            if (event.getKind() != FileEventKind.CREATE) return;
            @NotNull File file = event.getFile();
            if (file.isDirectory()) return;
            @NotNull String fileName = file.getName();
            if (!this.commands.contains(fileName)) return;
            int attempt = 0;
            while(!file.delete() && attempt++ < 10);
            bootstrap.processCommand(fileName);
        });
        watcher.startWatch();
    }

    @SneakyThrows
    private void prepareFolder(@NotNull final File folder) {
        if (!folder.exists()) {
            folder.mkdir();
            return;
        }
        @NotNull DirectoryStream.Filter<Path> filter = entry -> !Files.isDirectory(entry);
        try (@NotNull final DirectoryStream<Path> directoryStream =
                     Files.newDirectoryStream(folder.toPath(), filter)) {
            for (@NotNull final Path entry : directoryStream) {
                Files.delete(entry);
            }
        }
    }

    public void start() {
        if (watcher == null)
            init();
    }

    public void stop() {
        if (watcher != null)
            watcher.stopWatch();
    }

}
