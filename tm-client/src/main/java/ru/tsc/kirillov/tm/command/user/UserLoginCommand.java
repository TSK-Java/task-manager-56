package ru.tsc.kirillov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.UserLoginRequest;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выполнение авторизации.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Авторизация]");
        System.out.println("Введите логин");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Введите пароль");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = getAuthEndpoint().login(request);
        setToken(response.getToken());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
