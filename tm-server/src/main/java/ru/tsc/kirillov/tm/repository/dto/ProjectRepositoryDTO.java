package ru.tsc.kirillov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
public class ProjectRepositoryDTO extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO() {
        super(ProjectDTO.class);
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null) return new String[]{};
        @NotNull final String jpql = String.format(
                "SELECT id FROM %s m WHERE m.userId = :userId",
                getModelName()
        );
        @NotNull final List<String> result = entityManager.createQuery(jpql, String.class)
                .setParameter("userId", userId)
                .getResultList();
        return result.toArray(new String[]{});
    }

}
