package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import ru.tsc.kirillov.tm.dto.model.UserDTO;

public abstract class AbstractUserTest extends AbstractTest {

    @NotNull
    protected UserDTO userTest;

    @NotNull
    protected UserDTO userAdmin;

    @NotNull
    protected String userId;

    @Before
    @Override
    public void initialization() {
        super.initialization();
        userTest = userService.create("test", "test", "test@domain.com");
        userId = userTest.getId();
        userAdmin = userService.create("admin", "admin", "admin@domain.com");
    }

}
