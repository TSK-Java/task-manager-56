package ru.tsc.kirillov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.IReceiverService;
import ru.tsc.kirillov.tm.listener.LoggerListener;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    IReceiverService receiverService;

    @NotNull
    @Autowired
    LoggerListener loggerListener;

    @SneakyThrows
    public void init() {
        receiverService.init(loggerListener);
    }

}
