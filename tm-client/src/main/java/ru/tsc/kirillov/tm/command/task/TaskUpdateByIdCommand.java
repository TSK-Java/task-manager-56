package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskUpdateByIdRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление задачи по ID]");
        System.out.println("Введите ID задачи:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken(), id, name, description);
        getTaskEndpoint().updateTaskById(request);
    }

}
