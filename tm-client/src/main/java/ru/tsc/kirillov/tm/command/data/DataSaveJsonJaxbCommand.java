package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataJsonJaxbSaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

@Component
public final class DataSaveJsonJaxbCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-json-jaxb";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из json файла (JAXB API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения из json файла (JAXB API)]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonJaxbSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
