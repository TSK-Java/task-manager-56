package ru.tsc.kirillov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.ITaskRepository;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository() {
        super(Task.class);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.user.id = :userid AND m.project.id = :projectId",
                getModelName()
        );
        return entityManager.createQuery(jpql, clazz)
                .setParameter("userid", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        removeAll(findAllByProjectId(userId, projectId));
    }

}
