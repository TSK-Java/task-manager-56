package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request =
                new ProjectUpdateByIndexRequest(getToken(), index, name, description);
        getProjectEndpoint().updateProjectByIndex(request);
    }

}
