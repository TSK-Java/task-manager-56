package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.kirillov.tm.dto.response.ApplicationAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение информации о разработчике.";
    }

    @Override
    public void execute() {
        System.out.println("\n[О клиенте]");
        System.out.println("Разработчик: " + getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getPropertyService().getAuthorEmail());

        System.out.println("\n[О сервере]");
        ApplicationAboutResponse response = getSystemEndpoint().getAbout(new ApplicationAboutRequest());
        System.out.println("Разработчик: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
